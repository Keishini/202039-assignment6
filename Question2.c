#include <stdio.h>

int fibonacciSeq(int);
int fibonacciSeq(int n) {

   if(n == 0) {
      return 0;
   }
	
   if(n == 1) {
      return 1;
   }
   return fibonacciSeq(n-1) + fibonacciSeq(n-2);
}

int  main() {

   int i,n;
	printf("enter lines:");
	scanf("%d",&i);
   for (n = 0; n <=i; n++) {
      printf("%d\n", fibonacciSeq(n));
   }
	
   return 0;
}
